#!/usr/bin/env node
const os = require('os');
const mqtt = require('mqtt');
const yargs = require('yargs');
const { exec } = require('child_process');
const url = require('url');
const fs = require('fs');
const path = require('path');

(async () => {
	const appName = `${require('./package.json').knownas}`;
	const hostname = os.hostname().toLowerCase();
	const brokerDefault = 'mqtt://mqtt';

	let confPath = path.join(os.homedir(), `.${appName}`);
	let logger = require('winston-wrap')('index.js', { file: null });

	let configuration = {};
	const topics = [];

	function puts(error, stdout, stderr) {
		if(stdout) {
			logger.info(stdout);
		}
		logger.info('Execution Done');
	}

	function executeShellCommand(topic, payload){
		const commands = configuration[topic];
		if(!commands) {
			logger.error(`Topic: ${topic} not present in config file`);
			return;
		}
		const command = commands[payload];
		if(!command) {
			logger.debug(`Topic: ${topic} received a payload (${payload}) not present in config file`);
			return;
		}
		logger.info(`Executing command: ${command} because topic: ${topic} received payload: ${payload}`);
		exec(command, puts);
	}

	/*
	 █████  ██████   ██████  ███████
	██   ██ ██   ██ ██       ██
	███████ ██████  ██   ███ ███████
	██   ██ ██   ██ ██    ██      ██
	██   ██ ██   ██  ██████  ███████
	*/
	const { argv } = yargs
		.usage(`Usage: ${appName} [-i] [-u] [-b <broker url>] [-c <config path>] [-l <log path>]`)
		.example(`${appName}`, `Use default mqtt broker (mqtt://mqtt:1883) and config.json file present in user config directory, eg: ${confPath}\nNo logs to file\n`)
		.example(`${appName} -i`, `Install at startup, with default mqtt broker (mqtt://mqtt:1883) and config.json file present in user config directory, eg: ${confPath}\nNo logs to file\n`)
		.example(`${appName} -i -l`, `Install at startup, with default mqtt broker (mqtt://mqtt:1883) and config.json file present in user config directory, eg: ${confPath}\nlogs are send to a file in user config directory.\n`)
		.example(`${appName} -b mqtt://myname:secretpasswd@mqtt:8883 -c config.json`, `Connect to broker 'mqtt', on port 8883 with config.json parameter file.\n`)
		.example(`${appName} -i -c /home/pi/config.json -l`, `Install at startup, using /home/pi/config.json file, log to file in user config directory, eg: ${confPath}\n`)
		.example(`${appName} -i -c Z:\\config.json`, `Install at startup, using Z:\\config.json file, no log recorded\n`)
		.example(`${appName} -u`, `Uninstall from system startup\n`)
		.option('install', {
			alias: 'i',
			describe: 'Install in System Startup',
		})
		.option('uninstall', {
			alias: 'u',
			describe: 'Uninstall of System Startup',
		})
		.option('broker', {
			alias: 'b',
			describe: 'MQTT broker url',
			default: brokerDefault,
		})
		.option('config', {
			alias: 'c',
			describe: 'Path to json config file',
		})
		.option('log', {
			alias: 'l',
			describe: `Log to file, let empty to use User config dir (eg: ${confPath})`,
		})
		.help();

	// set logFile if requested
	const logFile = await require('./lib/logSetup.js')(appName, argv);
	if(logFile) {
		logger=require('winston-wrap')('index.js', { file: { filename: logFile } });
	}

	// set configFile
	const configFile = await require('./lib/confSetup.js')(appName, logFile, argv);

	// set broker url
	let brokerUrl=argv.broker;
	
	// execute system setup actions if requested
	if((argv.install) || (argv.uninstall)){
		await require('./lib/sysSetup.js')(__dirname, appName, brokerUrl, configFile, logFile, argv);
		process.exit(0);
	}

	/*
	 ██████  ██████  ███    ██ ███████ ██  ██████
	██      ██    ██ ████   ██ ██      ██ ██
	██      ██    ██ ██ ██  ██ █████   ██ ██   ███
	██      ██    ██ ██  ██ ██ ██      ██ ██    ██
	 ██████  ██████  ██   ████ ██      ██  ██████
	*/
	// Loading config
	logger.debug(`Reading configuration from ${configFile}`);
	try {
		configuration = JSON.parse(fs.readFileSync(configFile).toString());
	} catch (e) {
		logger.error(`${configFile} is not a correctly formatted, please fix it and try again.`);
		process.exit(1);
	}

	logger.debug('conf : \n%O', configuration);

	Object.keys(configuration).forEach((key) => {
		const topic = key;
		topics.push(topic);
	});

	if(topics.length===0) {
		logger.error(`${configFile} contains no topics, please fix it. Subscribing to hostname topic as replacment (${hostname}).`);
		topics.push(hostname);
	}

	/*
	███    ███  ██████  ████████ ████████
	████  ████ ██    ██    ██       ██
	██ ████ ██ ██    ██    ██       ██
	██  ██  ██ ██ ▄▄ ██    ██       ██
	██      ██  ██████     ██       ██
	               ▀▀
	*/
	// Creating the MQTT Client
	// Parse url
	const mqtt_url = url.parse(brokerUrl);
	const auth = (mqtt_url.auth || ':').split(':');

	const clientId = `${hostname}-exec_${Math.random().toString(16).substr(2, 8)}`;
	logger.debug(`Creating MQTT client for: '${hostname}' with Id: '${clientId}'`);
	const options = {
		port: mqtt_url.port,
		host: mqtt_url.hostname,
		username: auth[0],
		password: auth[1],
		clientId: clientId,
		will: {
			topic: `exec/${hostname}/connection/state`,
			payload: 'offline',
			qos: 1,
			retain: true,
		},
	}
	const client = mqtt.connect(options);

	client.on('connect', () => {
		logger.info(`MQTT connected, subscribed to topics: ${topics}`);
		client.subscribe(topics);
		client.subscribe(`exec/${hostname}/connection/clientId`);
		client.publish(`exec/${hostname}/connection/clientId`, clientId, { qos: 1 });
	});

	client.on('message', (topic, message, packet) => {
		logger.info(`MQTT received message : '${message}' in topic : ${topic}`);
		if(topic===`exec/${hostname}/connection/clientId`) {
			if(message.toString()!==clientId) {
				logger.info(`MQTT received exit request`);
				logger.info(`own clientId: '${clientId}'`);
				process.exit(0);
			} else {
				logger.info(`own clientId: '${clientId}'`);
				logger.info(`This MQTT client is authorized`);
				client.publish(`exec/${hostname}/connection/state`, 'online', { qos: 1, retain: true });
			}
		} else {
			executeShellCommand(topic, message);
		}
	});


	client.on('reconnect', (packet) => {
		logger.debug(`MQTT reconnect : ${packet}`);
	});

	client.on('close', (packet) => {
		logger.debug(`MQTT Close : ${packet}`);
	});

	client.on('error', (packet) => {
		logger.debug(`MQTT Error : ${packet}`);
	});

	client.on('end', (packet) => {
		logger.debug(`MQTT End : ${packet}`);
	});

	// client.options.reconnectPeriod = 0;  // disable automatic reconnect
})();
