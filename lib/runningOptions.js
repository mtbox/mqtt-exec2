#!/usr/bin/env node
const os = require('os');
const fs = require('fs');
const { execSync } = require('child_process');
const path = require('path');

async function setup(dirname, appName) {
	const logger=require('winston-wrap')('runningSetup.js', { file: null });
	let options = {};
	if(os.platform()==='win32') {
		const startpathScript = path.join(dirname, 'scripts', `startpath.vbs`);
		logger.info('Requesting System Startup config');
		const startpath = execSync(`cscript	/nologo ${startpathScript} ${appName}`).toString().trim();
		logger.info(startpath);
		try {
			const array = fs.readFileSync(startpath).toString().split('\n');
			const regexp = /'{.*}/;

			for(let i in array) {
				if(array[i].match(regexp)) {
					Object.assign(options, JSON.parse(array[i].slice(1)));
				}
			}
		} catch (e) {
		}
		logger.info('Current service options are \n%O', options);
	} else if(os.platform()==='linux') {
		const servicepath = `/etc/systemd/system/${appName}.service`;
		logger.info('Requesting Systemd Service config');
		try {
			const array = fs.readFileSync(servicepath).toString().split('\n');
			const regexp = /#{.*}/;

			for(let i in array) {
				if(array[i].match(regexp)) {
					Object.assign(options, JSON.parse(array[i].slice(1)));
				}
			}
		} catch (e) {
		}
		logger.info('Current service options are \n%O', options);
	} else {
		logger.error(`System unknown, can't install app`);
		process.exit(1);
	}
	return options;
}

module.exports = setup;
