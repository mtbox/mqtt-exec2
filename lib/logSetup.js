const os = require('os');
const fs = require('fs');
const path = require('path');
const { prompt } = require('enquirer');

async function setup(appName, argv) {
	const logger=require('winston-wrap')('logSetup.js', { file: null });
	const hostname = os.hostname().toLowerCase();
	let confPath = path.join(os.homedir(), `.${appName}`);
	logger.debug('userinfo %O', os.userInfo());

	// identify logfile
	let logFile = null;
	if(argv.log===true) {
		logFile = path.join(confPath, `output-${hostname}.log`);
		logger.debug(`Default log file requested '${logFile}''`);
		if(argv.install && !fs.existsSync(logFile)) {
			const answer = await prompt({
				type: 'input',
				name: 'logFile',
				message: `Confirm Log Path ?`,
				initial: logFile,
			}).catch(err => {
				if(err) {
					logger.error(err)
				};
				logger.error('Aborted');
				process.exit(1);
			});
			logFile = answer.logFile;
		}
	} else if(typeof argv.log === 'string') {
		logFile = path.resolve(argv.log);
		logger.debug(`logFile : ${logFile}`);
	}

	if(logFile) {
		let logPath = path.dirname(logFile);
		if(!fs.existsSync(logPath)) {
			if(argv.install) {
				const answer = await prompt({
					type: 'confirm',
					name: 'create',
					message: `Log Directory '${logPath}' doesn't exists, create it ?`,
					initial: true,
				}).catch(err => {
					if(err) {
						logger.error(err)
					};
					logger.error('Aborted');
					process.exit(1);
				});
				if(!answer.create) {
					logger.error('Log to file deactivated');
					logFile = null;
					logPath = null;
				}
			} else {
				logger.error(`${logPath} doesn't exists, creating it.`);
			}
			if(logPath) {
				fs.mkdirSync(logPath);
			}
		}
	}
	return logFile;
}
module.exports = setup;
