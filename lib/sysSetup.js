#!/usr/bin/env node
const os = require('os');
const { execSync, spawnSync } = require('child_process');
const fs = require('fs');
const path = require('path');

function setup(dirname, appName, brokerUrl, configFile, logFile, argv) {
	const logger=require('winston-wrap')('sysSetup.js', { file: logFile ? { filename: logFile } : null });

	if(argv.install) {
		let logOption = '';
		if(logFile) {
			logOption = `-l ${logFile}`;
		}
		const args = `-b ${argv.broker} -c ${configFile} ${logOption}`;
		if(os.platform()==='win32') {
			const startupScript = path.join(dirname, 'scripts', `${appName}.vbs`);
			let globalCommandPath;
			try {
				globalCommandPath = execSync(`where ${appName}`).toString().trim();
			} catch (e) {
				if(!globalCommandPath) {
					logger.warn('Please install mqtt-exec globally first (npm install -g mqtt-exec2)');
					logger.warn('Aborted');
					process.exit(0);
				}
			}
			logger.info(`global command path: \n${globalCommandPath}`);
			fs.writeFileSync(startupScript,	`'{"broker": ${JSON.stringify(brokerUrl)}}
'{"log": ${JSON.stringify(logFile)}}
'{"config": ${JSON.stringify(configFile)}}
'DO NOT EDIT ABOVE LINES
CreateObject("Wscript.Shell").Run "${appName} ${args}", 0`);
			logger.info('Installing a shortcut in Startup Folder with following options:\n%s',
				`${appName} ${args}`);
			spawnSync(
				'wscript',
				[path.join(dirname, 'scripts', `install.vbs`),
					appName,
					startupScript,
				]
			);
			logger.info(`${appName} Installed`);
		} else if(os.platform()==='linux') {
			const serviceScript = path.join(dirname, 'scripts', `${appName}.service`);
			let globalCommandPath;
			try {
				globalCommandPath = execSync(`which ${appName}`).toString().trim();
			} catch (e) {
				if(!globalCommandPath) {
					logger.warn('Please install mqtt-exec globally first (npm install -g mqtt-exec2)');
					logger.warn('Aborted');
					process.exit(0);
				}
			}
			logger.info(`globalCommand: ${globalCommandPath}`);
			fs.writeFileSync(serviceScript,	`#{"broker": ${JSON.stringify(brokerUrl)}}
#{"log": ${JSON.stringify(logFile)}}
#{"config": ${JSON.stringify(configFile)}}
#DO NOT EDIT ABOVE LINES
[Unit]
Description=${appName}
Wants=network-online.target
After=network.target

[Service]
ExecStart=${globalCommandPath} ${args}
WorkingDirectory=${dirname}
StandardOutput=inherit
StandardError=inherit
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target`);
			logger.info('Installing mqtt-exec.service with following options:\n %s',
				`${appName} ${args}`);
			console.log(execSync(path.join(dirname, 'scripts', 'install.sh')).toString());
			logger.info(`${appName} Installed`);
		} else {
			logger.error(`System unknown, can't install app`);
			process.exit(1);
		}
	}

	if(argv.uninstall) {
		if(os.platform()==='win32') {
			logger.info('Removing shortcut of Startup Folder');
			spawnSync(
				'wscript',
				[path.join(dirname, 'scripts', 'uninstall.vbs'),
					appName,
				]
			);
		} else if(os.platform()==='linux') {
			logger.info('Uninstalling mqtt-exec.service');
			console.log(execSync(path.join(dirname, 'scripts', 'uninstall.sh')).toString());
		}	else {
			logger.error(`System unknown, can't uninstall app`);
			process.exit(1);
		}
	}
}

module.exports = setup;
