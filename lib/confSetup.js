const os = require('os');
const fs = require('fs');
const path = require('path');
const { prompt } = require('enquirer');

async function setup(appName, logFile, argv) {
	const logger=require('winston-wrap')('confSetup.js', { file: logFile ? { filename: logFile } : null });
	const hostname = os.hostname().toLowerCase();

	// determin configFile
	let confPath = path.join(os.homedir(), `.${appName}`);
	let configFile = argv.config;
	if(!configFile) {
		configFile = path.join(confPath, 'config.json');
		logger.debug(`Default config file requested '${configFile}'`);
		if(argv.install && !fs.existsSync(configFile)) {
			const answer = await prompt({
				type: 'input',
				name: 'configFile',
				message: `Confirm Config Path ?`,
				initial: configFile,
			}).catch(err => {
				if(err) {
					logger.error(err)
				};
				logger.error('Aborted');
				process.exit(1);
			});
			configFile = answer.configFile;
		}
	}

	configFile = path.resolve(configFile);
	confPath = path.dirname(configFile);
	if(!fs.existsSync(confPath)) {
		if(argv.install) {
			const answer = await prompt({
				type: 'confirm',
				name: 'create',
				message: `Config Directory '${confPath}' doesn't exists, create it ?`,
				initial: true,
			}).catch(err => {
				if(err) {
					logger.error(err)
				};
				logger.error('Aborted');
				process.exit(1);
			});
			if(!answer.create) {
				logger.error('Config file is required');
				logger.error('Aborted');
				process.exit(1);
			}
		} else {
			logger.error(`${confPath} doesn't exists, creating it.`);
		}
		fs.mkdirSync(confPath);
	}
	if(!fs.existsSync(configFile)) {
		const defaultConfig = `{
	"exec/${hostname}":	{
		"test" : "echo 'Please, set your own commands'"
	}
}`;
		if(argv.install) {
			const answer = await prompt({
				type: 'confirm',
				name: 'create',
				message: `Config file '${configFile}' doesn't exists, create the following default one :\n${defaultConfig}\nOK ?`,
				initial: true,
			}).catch(err => {
				if(err) {
					logger.error(err)
				};
				logger.error('Aborted');
				process.exit(1);
			});
			if(!answer.create) {
				logger.error('Config file is required');
				logger.error('Aborted');
				process.exit(1);
			}
		} else {
			logger.error(`${configFile} doesn't exists, creating a default one. Please edit it.`);
		}

		fs.writeFileSync(configFile, defaultConfig);
	}

	return configFile;
}
module.exports = setup;
