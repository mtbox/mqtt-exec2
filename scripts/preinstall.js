#!/usr/bin/env node
const os = require('os');
const url = require('url');
const path = require('path');
let mqtt;
let logger;
try {
	mqtt = require('mqtt');
	logger=require('winston-wrap')('preinstall.js', { file: null });
} catch (e) {
	process.exit(0);
}

const hostname = os.hostname().toLowerCase();
const appName = `${require('../package.json').knownas}`;
const runningOptions = require('../lib/runningOptions.js')(path.join(__dirname, '..'), appName);

// Creating the MQTT Client
// Parse url
let brokerUrl = 'mqtt://mqtt';
if(runningOptions.broker) {
	brokerUrl = runningOptions.broker;
}
const mqtt_url = url.parse(brokerUrl);
const auth = (mqtt_url.auth || ':').split(':');

const clientId = `${hostname}-exec_${Math.random().toString(16).substr(2, 8)}`;
logger.debug(`Creating MQTT client for: '${hostname}' with Id: '${clientId}'`);
const options = {
	port: mqtt_url.port,
	host: mqtt_url.hostname,
	username: auth[0],
	password: auth[1],
	clientId: clientId,
}
const client = mqtt.connect(options);

client.on('connect', () => {
	logger.info(`MQTT connected, requesting old client to exit`);
	client.subscribe(`exec/${hostname}/connection/clientId`);
	client.publish(`exec/${hostname}/connection/clientId`, clientId, { qos: 1 });
});

client.on('message', (topic, message, packet) => {
	logger.info(`MQTT received message : '${message}' in topic : ${topic}`);
	if(topic===`exec/${hostname}/connection/clientId`) {
		if(message.toString()===clientId) {
			logger.info(`MQTT broker received exit request, we can continue.`);
			process.exit(0);
		}
	}
});
