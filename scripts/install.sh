#!/bin/bash
MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized

if ! sudo test -d /etc/systemd/system
then
	echo "Platform has no systemd, can't install"
	exit 1
else
	echo "Installing systemd service"
fi

sudo mv $MY_PATH/mqtt-exec.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl restart mqtt-exec
sudo systemctl enable mqtt-exec
