#!/bin/bash
MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized

if ! sudo test -d /etc/systemd/system
then
	echo "Platform has no systemd, can't install"
	exit 1
else
	echo "Uninstalling systemd service"
fi

sudo systemctl stop mqtt-exec
sudo systemctl disable mqtt-exec
sudo rm /etc/systemd/system/mqtt-exec.service
