**master at:** anhileK\workspace_node\mqtt-exec2

# mqtt-exec2

[![](https://badgen.net/packagephobia/install/mqtt-exec2)](https://packagephobia.now.sh/result?p=mqtt-exec2) [![](https://badgen.net/packagephobia/publish/mqtt-exec2)](https://packagephobia.now.sh/result?p=mqtt-exec2)


A MQTT agent that subscribes to a list of MQTT topics and executes a script/command each time a message arrives.

## Setup

	npm install -g mqtt-exec2

On linux you will need to install node with a [version manager](https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally) to install `mqtt-exe` with a non-root user.

## Usage

```
exec-mqtt --help

Usage: mqtt-exec [-i] [-u] [-b <broker url>] [-c <config path>] [-l <log path>]

Options:
  --version        Show version number                                 [boolean]
  --install, -i    Install in System Startup
  --uninstall, -u  Uninstall of System Startup
  --broker, -b     MQTT broker url                      [default: "mqtt://mqtt"]
  --config, -c     Path to json config file
  --log, -l        Log to file, let empty to use User config dir
                   (eg: C:\Users\mtbox\.mqtt-exec)
  --help           Show help                                           [boolean]

	Examples:
	  mqtt-exec                                 Use default mqtt broker
	                                            (mqtt://mqtt:1883) and config.json
	                                            file present in user config
	                                            directory, eg:
	                                            C:\Users\mtbox\.mqtt-exec
	                                            No logs to file

	  mqtt-exec -i                              Install at startup, with default
	                                            mqtt broker (mqtt://mqtt:1883) and
	                                            config.json file present in user
	                                            config directory, eg:
	                                            C:\Users\mtbox\.mqtt-exec
	                                            No logs to file

	  mqtt-exec -i -l                           Install at startup, with default
	                                            mqtt broker (mqtt://mqtt:1883) and
	                                            config.json file present in user
	                                            config directory, eg:
	                                            C:\Users\mtbox\.mqtt-exec
	                                            Logs are send to a file in user
	                                            config directory.

	  mqtt-exec -b                              Connect to broker 'mqtt', on port
		mqtt://myname:secretpasswd@mqtt:8883 -c   8883 with config.json parameter      
	  config.json                               file.

	  mqtt-exec -i -c /home/pi/config.json -l   Install at startup, using
	                                            /home/pi/config.json file, log to
	                                            file in user config directory,
	                                            eg: C:\Users\mtbox\.mqtt-exec

	  mqtt-exec -i -c Z:\config.json            Install at startup, using
	                                            Z:\config.json file, no log recorded

	  mqtt-exec -u                              Uninstall from system startup
```

## Configuration

Create a configuration file "config.json" and put it in your user folder (`~/.mqtt-exec` for linux or `%userprofile%\.mqtt-exec` for windows).

*Windows example*
```
{
  "exec/mypc":	{
    "foobar2000" : "\"K:/Program Files/foobar2000/foobar2000.exe\"",
    "shutdown" : "\"C:/Windows/System32/shutdown.exe\" /s",
    "calc" : "calc"
  }
}
```

*Linux example*
```
{
  "/home/devices/livingroom/light1/value/set":  {
    "true"  : "sudo /home/pi/rcswitch-pi/sendRev B 1 1",
    "false" : "sudo /home/pi/rcswitch-pi/sendRev B 1 0"
  },
  "/home/devices/livingroom/light2/value/set":  {
    "true"  : "sudo /home/pi/rcswitch-pi/sendRev B 2 1",
    "false" : "sudo /home/pi/rcswitch-pi/sendRev B 2 0"
  }
}
```

## Credit

Thank to Dennis Schulte creator of [mqtt-exec](https://github.com/denschu/mqtt-exec). It's config.json concept is re-used here.

## Licence

Copyright © 2018-present, macro-toolbox.com. Released under the MIT License.
